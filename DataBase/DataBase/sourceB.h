#ifndef _SOURCEB_H_
#define _SOURCEB_H_

#include <sstream>
#include "sqlite3.h"
#include <iostream>
using namespace std;


bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
int getPrice(void* price, int argc, char** argv, char** azCol);
int getBalance(void* balance, int argc, char** argv, char** azCol);
int getAvailable(void* available, int argc, char** argv, char** azCol);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

#endif