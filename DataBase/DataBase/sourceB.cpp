#include "sourceB.h"


int main()
{

	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	stringstream *s;
	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	carPurchase(1, 8, db, zErrMsg);
	carPurchase(2, 4, db, zErrMsg);
	carPurchase(6, 16, db, zErrMsg);
	balanceTransfer(1, 2, 2000, db, zErrMsg);
	sqlite3_close(db);
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int available = 0;
	int price = 0;
	bool ans = false;
	int balance = 0;
	stringstream s("");
	sqlite3_exec(db,"begin transaction", NULL, NULL, &zErrMsg);
	s << "Select available from cars where id =" << carid;
	sqlite3_exec(db, s.str().c_str(), getAvailable, &available, &zErrMsg);
	if (available == 1)
	{
		s.str(std::string());
		s << "Select price from cars where id =" << carid;
		sqlite3_exec(db, s.str().c_str(), getPrice, &price, &zErrMsg);
		s.str(std::string());
		s << "Select Balance from Accounts where Buyer_id =" << buyerid;
		sqlite3_exec(db, s.str().c_str(), getBalance, &balance, &zErrMsg);
		if (balance >= price)
		{
			s.str(std::string());
			s << "update Accounts set balance = " << balance - price << " where  Buyer_id = " << buyerid;
			sqlite3_exec(db, s.str().c_str() , NULL, NULL, &zErrMsg);
			s.str(std::string());
			s << "update cars set available = " << 0 << " where id = " << carid;
			sqlite3_exec(db, s.str().c_str(), NULL, NULL, &zErrMsg);
			ans = true;
		}
	}
	sqlite3_exec(db, "commit", NULL, NULL, &zErrMsg);
	return ans;
}

int getPrice(void* price, int argc, char** argv, char** azCol)
{
	int* price2 = (int*)price;
	*price2 = atoi(argv[0]);
	return 0;
}

int getBalance(void* balance , int argc, char** argv, char** azCol)
{
	int* balance2 = (int*)balance;
	*balance2 = atoi(argv[0]);
	return 0;
}

int getAvailable(void* available, int argc, char** argv, char** azCol)
{
	int* available2 = (int*)available;
	*available2 = atoi(argv[0]);
	return 0;
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	bool ans = false;
	int toBalance;
	int fromBalance;
	stringstream s("");
	sqlite3_exec(db, "begin transaction", NULL, NULL, &zErrMsg);
	s << "Select balance from Accounts where Buyer_id =" << from;
	sqlite3_exec(db, s.str().c_str(), getBalance, &fromBalance, &zErrMsg);
	if (fromBalance >= amount)
	{
		s.str(std::string());
		s << "Select balance from Accounts where Buyer_id =" << to;
		sqlite3_exec(db, s.str().c_str(), getBalance, &toBalance, &zErrMsg);
		s.str(std::string());
		s << "update Accounts set balance = " << toBalance + amount << " where Buyer_id = " << to;
		sqlite3_exec(db, s.str().c_str(), NULL, NULL, &zErrMsg);
		s.str(std::string());
		s << "update Accounts set balance = " << fromBalance - amount << " where Buyer_id = " << from;
		sqlite3_exec(db, s.str().c_str(), NULL, NULL, &zErrMsg);
		ans = true;
	}
	sqlite3_exec(db, "commit", NULL, NULL, &zErrMsg);
	return ans;
}